# Codebase release 0.2 for Pychastic

by Radost Waszkiewicz, Maciej Bartczak, Kamil Kolasa, Maciej Lisicki

SciPost Phys. Codebases 11-r0.2 (2023) - published 2023-04-03

[DOI:10.21468/SciPostPhysCodeb.11-r0.2](https://doi.org/10.21468/SciPostPhysCodeb.11-r0.2)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.11-r0.2) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Radost Waszkiewicz, Maciej Bartczak, Kamil Kolasa, Maciej Lisicki

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.11-r0.2](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.11-r0.2)
* Live (external) repository at [https://github.com/RadostW/stochastic/tree/cb736b49381157993df346a9273647a2e1e47093](https://github.com/RadostW/stochastic/tree/cb736b49381157993df346a9273647a2e1e47093)
